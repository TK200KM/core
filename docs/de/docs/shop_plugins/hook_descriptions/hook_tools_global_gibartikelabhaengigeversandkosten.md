# HOOK_TOOLS_GLOBAL_GIBARTIKELABHAENGIGEVERSANDKOSTEN (162)

## Triggerpunkt

Am Anfang der Ermittlung der artikelabhängigen Versandkosten

## Parameter

* `JTL\Catalog\Product\Artikel` **&oArtikel** - Artikelobjekt
* `int` **&cLand** - Landes-ID
* `int` **&nAnzahl** - Anzahl
* `bool` **&bHookReturn** - Hook-Resultat (default `= false`)