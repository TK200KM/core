# HOOK_BOXEN_INC_NEWSKATEGORIE (94)

## Triggerpunkt

Vor der Anzeige der News-Kategorie

## Parameter

* `JTL\Boxes\Items\NewsCategories` **&box** - News-Kategorie-Objekt
* `array` **&cache_tags** - Umfang des Zwischenspeicherns
* `bool` **cached** - Flag "wurde vom Zwischenspeicher gelesen?"