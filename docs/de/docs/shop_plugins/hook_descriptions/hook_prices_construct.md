# HOOK_PRICES_CONSTRUCT (260)

## Triggerpunkt

Beim Erstellen der Preise, in `Preise::__construct()`

## Parameter

* `int` **customerGroupID** - Kundengruppen-ID
* `int` **customerID** - Kunden-ID
* `int` **productID** - Artikel-ID
* `int` **taxClassID** - Steuer-ID
* `JTL\Catalog\Product\Preise` **prices** - Preis-Objekt